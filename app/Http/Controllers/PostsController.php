<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    public function item1(Post $posts){

        return view('Item.Item1', compact('posts'));

    }
    public function activ(Post $posts){

        return view('Item.Item_activ', compact('posts'));

    }
    public function hover(Post $posts){

        return view('Item.Item_hover', compact('posts'));

    }


    public function store(){
        // validate

        $this->validate(request(),[
            'title' => 'required|min:4|unique:posts,title',
            'slug' => 'required|min:2|max:20|unique:posts,slug',
            'body' => 'required|min:20'
        ]);

        // save

        Post::create(request(['title','slug','body']));

        //redirect to home page

        return redirect('/');
    }
}