@extends('template')

@section('content')
    @foreach($posts as $post)
        <div class="col-md-12">
            <h2>{{ $post['title'] }}</h2>
            <p>{{ $post['body'] }} </p>
        </div>
    @endforeach
@endsection