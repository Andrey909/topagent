
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>TopAgent</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="jumbotron.css" rel="stylesheet">
</head>

<body>



<main role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-3">Travel</h1>
            <div class="col-md-5">

                <form action="/posts" method="post" class="form-horizontal">



                    {{csrf_field()}}

                    <div class="form-group">

                        <label for="title">Title:</label>
                        <input type="text" name="title" id="title" class="form-control">

                    </div>

                    <div class="form-group">

                        <label for="slug">Slug:</label>
                        <input type="text" name="slug" id="slug" class="form-control">

                    </div>

                    <div class="form-group">

                        <label for="body">Body:</label>
                        <textarea name="body" id="body" class="form-control"></textarea>

                    </div>

                    <div class="form-group">
                        <button class="btn btn-default">Create</button>
                    </div>

                </form>

            </div>

            <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Item</a>
            <div class="dropdown-menu" aria-labelledby="dropdown01">
                <a class="dropdown-item" href="/item1">Item1</a>
                <a class="dropdown-item" href="/item_activ">item_activ</a>
                <a class="dropdown-item" href="/item_hover">item_hover</a>

            </div>
        </div>



    </div>

    <div class="container">
        <!-- Example row of columns -->
        <div class="row">

            @yield('content')

        </div>

        <hr>

    </div> <!-- /container -->

</main>

<footer class="container">
    <p>&copy; 2018</p>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>

</body>
</html>
